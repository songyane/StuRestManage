from django.db import models


class Canteen(models.Model):
    CID = models.AutoField(primary_key=True)
    name = models.CharField(max_length=16, verbose_name="食堂名", unique=True)
    # stu = models.ManyToManyField("Login", null=True)
    secret = models.CharField(max_length=128, null=False, blank=True)

    # canteen_store = models.ManyToManyField("Store", null=True, blank=True)

    class Meta:
        db_table = "canteen"
        verbose_name = "食堂"
        verbose_name_plural = "食堂"

    def __str__(self):
        return self.name


class Login(models.Model):
    LID = models.CharField(primary_key=True, max_length=64, verbose_name="登录ID")
    username = models.CharField(max_length=32)

    class Meta:
        db_table = "login"


    def __str__(self):
        return self.username


class Store(models.Model):
    SID = models.AutoField(primary_key=True)
    StoreName = models.CharField(max_length=32, verbose_name="店铺名")
    food = models.ManyToManyField("FoodList", null=True, blank=True)
    canteen_store = models.ForeignKey("Canteen", null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        db_table = "store"
        verbose_name = "店铺"
        verbose_name_plural = "店铺"

    def __str__(self):
        return self.StoreName


# 店铺申请
class ApplyItem(models.Model):
    DianPU = models.CharField(max_length=32, verbose_name="店铺名")
    name = models.CharField(max_length=16, verbose_name="食堂名")
    Description = models.TextField(verbose_name="店铺申请理由")
    Status = models.CharField(max_length=10, verbose_name="申请状态", default="申请中")     # 1.申请中 2.未通过 3.通过

    class Meta:
        db_table = "apply_item"
        verbose_name = "店铺创建申请"
        verbose_name_plural = "店铺创建申请"

    def __str__(self):
        return self.DianPU + " " + self.Status


class FoodList(models.Model):
    FID = models.BigAutoField(primary_key=True)
    FName = models.CharField(max_length=32, verbose_name="菜名")
    Price = models.CharField(max_length=16, verbose_name="价格")
    Image = models.FileField(max_length=128, verbose_name="菜图片", upload_to='img')

    class Meta:
        db_table = "foodlist"
        verbose_name = "商品"
        verbose_name_plural = "商品"

    def __str__(self):
        return self.FName
