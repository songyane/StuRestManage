from django.contrib import admin
from . import models
# Register your models here.

admin.site.site_title = "食堂管理"
admin.site.site_header = "食堂管理"
admin.site.index_title = "食堂管理"

admin.site.register([models.FoodList,models.ApplyItem])
admin.site.register(models.Store)
admin.site.register(models.Canteen)
